use twapi_ureq::*;
use std::env;

fn main() {
    // OAuth1.0 Authentication
    let consumer_key = env::var("CONSUMER_KEY").unwrap();
    let consumer_secret = env::var("CONSUMER_SECRET").unwrap();
    let access_key = env::var("ACCESS_KEY").unwrap();
    let access_secret = env::var("ACCESS_SECRET").unwrap();

    // Tweet Lookup
    let url = "https://api.twitter.com/2/tweets/1330918993419354121";
    let query_options = vec![
        ("expansions", "attachments.media_keys"),
        ("media.fields", "duration_ms,height,media_key,preview_image_url,type,url,width,public_metrics"),
        //("media.fields", "duration_ms,height,media_key,preview_image_url,type,url,width,public_metrics,non_public_metrics,organic_metrics,promoted_metrics"),
    ];
    let res = v1::get(
        url,
        &query_options,
        &consumer_key,
        &consumer_secret,
        &access_key,
        &access_secret,
    );
    //print_headers(&res);
    println!("{}", res.into_json().unwrap().to_string());
    
}

#[allow(dead_code)]
fn print_headers(response: &twapi_ureq::ureq::Response) {
    for name in response.headers_names() {
        println!("{}={}", name, response.header(&name).unwrap_or(""));
    }
}