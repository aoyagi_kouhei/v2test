use twapi_ureq::*;
use std::env;

fn main() {
    // OAuth1.0 Authentication
    let consumer_key = env::var("CONSUMER_KEY").unwrap();
    let consumer_secret = env::var("CONSUMER_SECRET").unwrap();
    let access_key = env::var("ACCESS_KEY").unwrap();
    let access_secret = env::var("ACCESS_SECRET").unwrap();

    // Tweet Lookup
    let url = "https://api.twitter.com/2/tweets/1072349371705057281";
    let query_options = vec![
//        ("expansions", "author_id,attachments.media_keys"),
//        ("user.fields", "created_at"),
//        ("tweet.fields", "lang"),
//        ("place.fields", "name"),
 //       ("tweet.fields", "context_annotations,entities")
    ];
    let res = v1::get(
        url,
        &query_options,
        &consumer_key,
        &consumer_secret,
        &access_key,
        &access_secret,
    );
    //print_headers(&res);
    println!("{}", res.into_json().unwrap().to_string());
    
}

#[allow(dead_code)]
fn print_headers(response: &twapi_ureq::ureq::Response) {
    for name in response.headers_names() {
        println!("{}={}", name, response.header(&name).unwrap_or(""));
    }
}